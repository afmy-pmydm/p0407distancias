package net.iescierva.dam20_18.p0407distancias;

public class GeoException extends Exception {
    public GeoException(String e) {
        super(e);
    }
};
