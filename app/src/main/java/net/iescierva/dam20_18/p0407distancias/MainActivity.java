package net.iescierva.dam20_18.p0407distancias;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.appcompat.app.AppCompatActivity;

import net.iescierva.dam20_18.p0407distancias.R;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    //Acceso a Punto 1
    EditText p1LatG,p1LatM,p1LatS;
    EditText p1LonG,p1LonM,p1LonS;
    ToggleButton p1LatMinus, p1LonMinus;
    //Acceso a Punto 2
    EditText p2LatG,p2LatM,p2LatS;
    EditText p2LonG,p2LonM,p2LonS;
    ToggleButton p2LatMinus, p2LonMinus;
    Dialog textoPunto1;
    Dialog textoPunto2;

    TextView distancia;

    Button btnCalcular,btnBorrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        p1LatMinus=findViewById(R.id.p1LatMinus);
        p1LonMinus=findViewById(R.id.p1LonMinus);
        p1LatG=findViewById(R.id.p1latG);
        p1LatM=findViewById(R.id.p1latM);
        p1LatS=findViewById(R.id.p1latS);
        p1LonG=findViewById(R.id.p1lonG);
        p1LonM=findViewById(R.id.p1lonM);
        p1LonS=findViewById(R.id.p1lonS);
        //Acceso a Punto 2
        p2LatMinus=findViewById(R.id.p2LatMinus);
        p2LonMinus=findViewById(R.id.p2LonMinus);
        p2LatG=findViewById(R.id.p2latG);
        p2LatM=findViewById(R.id.p2latM);
        p2LatS=findViewById(R.id.p2latS);
        p2LonG=findViewById(R.id.p2lonG);
        p2LonM=findViewById(R.id.p2lonM);
        p2LonS=findViewById(R.id.p2lonS);

        distancia=findViewById(R.id.distancia);

        btnCalcular=findViewById(R.id.btnCalcular);
        btnBorrar=findViewById(R.id.btnBorrar);

        btnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                p1LatMinus.setChecked(false);
                p1LonMinus.setChecked(false);
                p1LatG.setText("");
                p1LatM.setText("");
                p1LatS.setText("");
                p1LonG.setText("");
                p1LonM.setText("");
                p1LonS.setText("");

                p2LatMinus.setChecked(false);
                p2LonMinus.setChecked(false);
                p2LatG.setText("");
                p2LatM.setText("");
                p2LatS.setText("");
                p2LonG.setText("");
                p2LonM.setText("");
                p2LonS.setText("");
            }
        });

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double latitudGradosPunto1 = Double.parseDouble(p1LatG.getText().toString());
                double latitudMinutosPunto1 = Double.parseDouble(p1LatM.getText().toString());
                double latitudSegundosPunto1 = Double.parseDouble(p1LatS.getText().toString());
                try {
                    GeoPunto p1=new GeoPunto(
                            p1LatG.getText().toString().isEmpty()? 0: latitudGradosPunto1,
                            p1LatM.getText().toString().isEmpty()? 0: latitudMinutosPunto1,
                            p1LatS.getText().toString().isEmpty()? 0: latitudSegundosPunto1,
                            p1LonG.getText().toString().isEmpty()? 0: Double.parseDouble(p1LonG.getText().toString()),
                            p1LonM.getText().toString().isEmpty()? 0: Double.parseDouble(p1LonM.getText().toString()),
                            p1LonS.getText().toString().isEmpty()? 0: Double.parseDouble(p1LonS.getText().toString())
                    );
                    if (p1LatMinus.isChecked()) p1.setLatitud((-1)*p1.getLatitud());
                    if (p1LonMinus.isChecked()) p1.setLongitud((-1)*p1.getLongitud());

                    GeoPunto p2=new GeoPunto(
                            p2LatG.getText().toString().isEmpty()? 0: Double.parseDouble(p2LatG.getText().toString()),
                            p2LatM.getText().toString().isEmpty()? 0: Double.parseDouble(p2LatM.getText().toString()),
                            p2LatS.getText().toString().isEmpty()? 0: Double.parseDouble(p2LatS.getText().toString()),
                            p2LonG.getText().toString().isEmpty()? 0: Double.parseDouble(p2LonG.getText().toString()),
                            p2LonM.getText().toString().isEmpty()? 0: Double.parseDouble(p2LonM.getText().toString()),
                            p2LonS.getText().toString().isEmpty()? 0: Double.parseDouble(p2LonS.getText().toString())
                    );
                    if (p2LatMinus.isChecked()) p2.setLatitud((-1)*p2.getLatitud());
                    if (p2LonMinus.isChecked()) p2.setLongitud((-1)*p2.getLongitud());

                    double result = p1.distancia(p2)/1000;
                    Locale l=Locale.getDefault();
                    distancia.setText(String.format(l,"%10.2f", result)+" km");
                }
                catch (Exception e) {
                   /* ejemplo de flag de errores
                   if (p1LatG.getText().toString().matches(""))
                          p1LatG.setError(getResources().getString(R.string.error_valor_incorrecto));
                   */
                    distancia.setText(getResources().getString(R.string.error_generico));
                }
            }
        });
    }
}